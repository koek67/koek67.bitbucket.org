/********************************************************
*														*
* 	hotel priming dashboard		*
* 	Koushik Krishnan 18th May 2015 						*
*														*
********************************************************/

/********************************************************
*														*
* 	Load data from json file						*
*														*
********************************************************/
d3.json("../data/priming_status.json", function (data) {

  /********************************************************
  *														*
  * 	Format the data and initialize global variables
  *														*
  ********************************************************/
  var delay = 1000; // time for all transitions (ms)

  /*
   * Collect a list of all the names of the primers. This
   * will be used as the x-axis for the bar chart.
   */
  var names = []
  data.forEach(function (value) {
    var name = value['primer']
    var contain = false;
    // check to see if this primer already exists in 'names'
    names.forEach( function (d) {
      if (d === name)
        contain = true
    })
    if (!contain)
      names.push(name)
  })
  names.sort()

  /*
   * Create a date formatting function and format the dates
   * in the data so that they can be sorted numerically.
   * In addition, make life easy and shorten the column names.
   */
  var dateFormat = d3.time.format('%Y-%m-%d')
  data.forEach(function(d){
    d.dd = dateFormat.parse(d.date)
    d.expected = d['priming_queries_generated']
    d.executed = d['priming_queries_executed']
    d.successful = d['priming_queries_successful']
  })


  /********************************************************
  *														*
  * 	Create the dc.js chart objects & link to html	*
  *														*
  ********************************************************/
  var barChart = dc.barChart("#morris-area-chart")
  var barWidth = $(document).width() * 0.623611
  var barHeight = $(document).height() * 0.215

  var dayChart = dc.lineChart('#day-chart')
  var bubbleChart = dc.bubbleChart('#bubble-chart')
  var arcChart = dc.pieChart('#arc-chart')
  var dataTable = dc.dataTable('#data-table')


  /********************************************************
  *														*
  * 	Run data through crossfilter				*
  *														*
  ********************************************************/
  var ndx = crossfilter(data)

  /********************************************************
  *														*
  * 	Create Dimensions that we'll need			*
  *														*
  ********************************************************/
  var tableDimension = ndx.dimension(function(d) { d.dd; })

  var arcDimension = ndx.dimension(function(d) {
    var val = d['available_request_capacity']
    if (val < 0) return "under"
    if (val == 0) return "equal"
    else return "over"
  })

  var dayDimension = ndx.dimension(function(d) { return d.dd; })

  var bubbleDimension = ndx.dimension(function(d) { return d.primer; })

  var primerDimension = ndx.dimension(function(d) {
    return d.primer
  })

  /********************************************************
  *														*
  * 	Define the grouping and reducing functions for each
  *   dimension.
  *														*
  ********************************************************/

  var tableDimensionGroup = tableDimension.group()

  var arcDimensionGroup = arcDimension.group()

  var dayGroup = dayDimension.group().reduce(
    function(p, v) { p.exec += v.executed; return p; },
    function(p, v) { p.exec -= v.executed; return p; },
    function(p, v) { return {exec: 0} }
  )

  var bubbleDimensionGroup = bubbleDimension.group().reduce(
    // add
    function(p, v) {
      p.inside_no += v['h_queries_in_prime_window_not_cached']
      p.inside_yes += v['h_queries_in_prime_window_cached']
      p.outside_no += v['h_queries_outside_prime_window_not_cached']
      p.outside_yes += v['h_queries_outside_prime_window_cached']

      // calc the rest
      p.inside = (p.inside_yes) / (p.inside_yes + p.inside_no)
      p.outside = (p.outside_yes) / (p.outside_yes + p.outside_no)
      return p
    },
    // remove
    function(p, v) {
      p.inside_no -= v['h_queries_in_prime_window_not_cached']
      p.inside_yes -= v['h_queries_in_prime_window_cached']
      p.outside_no -= v['h_queries_outside_prime_window_not_cached']
      p.outside_yes -= v['h_queries_outside_prime_window_cached']

      // calc the rest
      p.inside = (p.inside_yes) / (p.inside_yes + p.inside_no)
      p.outside = (p.outside_yes) / (p.outside_yes + p.outside_no)
      return p
    },
    // init
    function(p, v) {
      return {inside_no: 0, inside_yes: 0, outside_no: 0, outside_yes: 0, inside: 0, outside: 0}
    }
  )

  var primerDimensionGroup = primerDimension.group().reduce(
    // add
    function(p, v) {
      // recalculate raw values
      p.expected += parseInt(v.expected)
      p.executed += parseInt(v.executed)
      p.num_succ += parseInt(v.successful)
      // recalculate percentages
      p.success = p.num_succ / p.expected
      p.fail = (p.executed - p.num_succ) / p.expected
      p.dropped = (p.expected - p.executed) / p.expected

      return p
    },
    // remove
    function(p, v) {
      // recalculate raw values
      p.expected -= parseInt(v.expected)
      p.executed -= parseInt(v.executed)
      p.num_succ -= parseInt(v.successful)
      // recalculate percentages
      p.success = p.num_succ / p.expected
      p.fail = (p.executed - p.num_succ) / p.expected
      p.dropped = (p.expected - p.executed) / p.expected

      return p
    },
    // init
    function(p, v) {
      return {expected: 0, executed: 0, num_succ: 0, success: 0, fail: 0, dropped: 0}
    }
  )

  // get the minimum and maximum expected number of queries
  var minExp = primerDimension.top()[0];
  var maxExp = primerDimension.bottom(1)[0].expected;


  /********************************************************
  *														*
  * 	Create the charts and define each's properties
  *														*
  ********************************************************/

  // bar chart: Number of Expected Queries per Primer
  barChart
    .width(barWidth)
    .height(barHeight)
    .margins({ top: 10, right: 50, bottom: 70, left: 60 })
    .dimension(primerDimension)
    .group(primerDimensionGroup)
    .valueAccessor(function(p) {
      return p.value.num_succ;
    })
    .stack(primerDimensionGroup, function(p) {
      return  p.value.executed - p.value.num_succ;
    })
    .stack(primerDimensionGroup, function(p) {
      return  p.value.expected - p.value.executed;
    })
    .x(d3.scale.ordinal().domain(names))
    .y(d3.scale.linear().domain([0, maxExp]))
    .renderHorizontalGridLines(true)
    .elasticY(true)
    .xUnits(dc.units.ordinal)
    .transitionDuration(delay)
    .renderlet(function(chart) {
      chart.selectAll('g.x text')
        .attr('dx', '-30')
        .attr('dy', '10')
        .attr('transform', 'rotate(-65)')
    })
    .brushOn(true)
    .title(function(d) {
      var info = d.key + "\n"
      info += "Success: " + numeral((d.value.success * 100)).format('0.00') + "% \n"
      info += "Dropped: " + numeral((d.value.dropped * 100)).format('0.00') + "% \n"
      info += "Failed: " + numeral((d.value.fail * 100)).format('0.00') + "%"
      return info;
    })
    .renderTitle(true)

  // line chart: Number of Queries Executed per day
  dayChart
    .width(480)
    .height(500)
    .transitionDuration(delay)
    .margins({ top: 20, right: 50, bottom: 75, left: 40 })
    .renderLabel(true)
    .dimension(dayDimension)
    .group(dayGroup)
    .valueAccessor(function(p){
      return p.value.exec;
    })
    .x(d3.time.scale().domain(d3.extent(data, function(d){ return d.dd; })))
    .xUnits(d3.time.days)
    .y(d3.scale.linear().domain(d3.extent(data, function(d){ return d.executed; })))
    .renderHorizontalGridLines(true)
    .renderlet(function(chart) {
      chart.selectAll('g.x text')
        .attr('dx', '-30')
        .attr('transform', 'rotate(-65)')
    })
    .xAxis().ticks(30)

    // bubble chart: Priming Window Performance by Primer
    bubbleChart
      .width(650)
     .height(300)
     .dimension(bubbleDimension)
     .group(bubbleDimensionGroup)
     .transitionDuration(delay)
     .x(d3.scale.linear().domain([0, 1]))
     .y(d3.scale.linear().domain([0, 1]))
     .r(d3.scale.linear().domain([0, 50]))
     .keyAccessor(function (p) {
       console.log('here');
       console.log(p);
       return p.value.inside;
     })
     .valueAccessor(function (p) {
       return p.value.outside;
     })
     .radiusValueAccessor(function (p) {
       return 1;
     })
     .label(function (p) {
       return p.key;
       }
     )

    // pie chart: Available Request Capacity
    arcChart
      .width(400)
      .height(200)
      .dimension(arcDimension)
      .group(arcDimensionGroup)

    // data table
    dataTable
      .width(barWidth)
      .dimension(tableDimension)
      .group(function(d) { return "(displaying only first 15 entries)" })
      .size(data.length / 2)
      .columns([
        function(d) { return d.dd },
        function(d) { return d['primer'] },
        function(d) { return d['provider'] },
        function(d) { return Math.round(d.expected) },
        function(d) { return Math.round(d.executed) },
        function(d) { return Math.round(d.successful) }
      ])
      .sortBy(function(d) { return d.dd })
      .order(d3.ascending)

  /********************************************************
  *														*
  * 	Render it all!
  *														*
  ********************************************************/
  dc.renderAll()
})
